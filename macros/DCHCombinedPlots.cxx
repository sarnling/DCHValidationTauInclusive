#include <map>
#include <utility>
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::ostringstream
#include "TFile.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TLine.h"
#include "TLegend.h"
#include "TROOT.h"

void DCHCombinedPlots() {
  gROOT->SetBatch(kTRUE);
  gStyle->SetOptStat(0);
 
  double m_dch[] = {300,400,500,600,700,800,900,1000,1100,1200,1300};
  
  //Integer to color the histogram lines
  int color = 1;
  
  //Validation Files
  std::map <std::string, TFile*> validationFiles;
  //Histograms
  std::map <std::string, TH1D*> h1;
  //std::map <std::string, TH1D*> h2;
  //std::map <std::string, TH1D*> h3;
  std::map <std::string, TH1D*> h4;
  std::map <std::string, TH1D*> h5;
  std::map <std::string, TH1D*> h6;
  std::map <std::string, TH1D*> h7;
  std::map <std::string, TH1D*> h8;
  std::map <std::string, TH1D*> h9;
  std::map <std::string, TH1D*> h10;
  std::map <std::string, TH1D*> h11;
  std::map <std::string, TH1D*> h12;
  std::map <std::string, TH1D*> h13;
  std::map <std::string, TH1D*> h14;

  TCanvas* c1 = new TCanvas("decayTypeC", "decayTypeC", 600, 400);
  //TCanvas* c2 = new TCanvas("leptonPairTypePC", "leptonPairTypePC", 600, 400);
  //TCanvas* c3 = new TCanvas("leptonPairTypeNC", "leptonPairTypeNC", 600, 400);
  TCanvas* c4 = new TCanvas("invariantMassPC", "invariantMassPC", 600, 400);
  TCanvas* c5 = new TCanvas("invariantMassNC", "invariantMassNC", 600, 400);
  TCanvas* c6 = new TCanvas("leadingPtPC", "leadingPtPC", 600, 400);
  TCanvas* c7 = new TCanvas("leadingPtNC", "leadingPtNC", 600, 400);
  TCanvas* c8 = new TCanvas("subLeadingPtPC", "subLeadingPtPC", 600, 400);
  TCanvas* c9 = new TCanvas("subLeadingPtNC", "subLeadingPtNC", 600, 400);
  TCanvas* c10 = new TCanvas("leadingEtaPC", "leadingEtaPC", 600, 400);
  TCanvas* c11 = new TCanvas("leadingEtaNC", "leadingEtaNC", 600, 400);
  TCanvas* c12 = new TCanvas("subLeadingEtaPC", "subLeadingEtaPC", 600, 400);
  TCanvas* c13 = new TCanvas("subLeadingEtaNC", "subLeadingEtaNC", 600, 400);
  TCanvas* c14 = new TCanvas("FinalStateLeptons", "Final State Leptons", 600, 400);

  TLegend* leg = new TLegend(0.15,0.53,0.48,0.88);
  leg->SetHeader("A14_NNPDF23LO_EvtGen");
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  TLegend* leg2 = new TLegend(0.67,0.53,1.00,0.88);
  leg2->SetHeader("A14_NNPDF23LO_EvtGen");
  leg2->SetFillStyle(0);
  leg2->SetBorderSize(0);
  
  TObject* nullPtr = NULL;  
  TLegend* legend = new TLegend(0.82,0.71,0.98,0.88);
  legend->AddEntry(nullPtr,"e  =  e/#tau_{e}    ","");
  legend->AddEntry(nullPtr,"m  =  #mu/#tau_{#mu}     ","");
  legend->AddEntry(nullPtr,"t  =  #tau_{had}    ","");
  
  for(int i = 0; i<11; i++){
   
    // Open file
    ostringstream fileName; fileName << "/afs/cern.ch/user/s/sarnling/DCHSignalSample/histograms/DCH" << m_dch[i] << "AtLeastOneTauFilter.Validation.histo.root";
    validationFiles.insert( std::pair<std::string,TFile*>( fileName.str(), TFile::Open( fileName.str().c_str() ) ) );
    // Get Histogram Pointers
    h1.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/DecayType" ) ) );
    //h2.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/LeptonPairTypeP" ) ) );
    //h3.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/LeptonPairTypeN" ) ) );
    h4.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/InvariantMassPGlobal" ) ) );
    h5.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/InvariantMassNGlobal" ) ) );
    h6.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/LeadingPtP" ) ) );
    h7.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/LeadingPtN" ) ) );
    h8.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/SubLeadingPtP" ) ) );
    h9.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/SubLeadingPtN" ) ) );
    h10.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/LeadingEtaP" ) ) );
    h11.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/LeadingEtaN" ) ) );
    h12.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/SubLeadingEtaP" ) ) );
    h13.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/SubLeadingEtaN" ) ) );
    h14.insert( std::pair<std::string,TH1D*>( fileName.str(), (TH1D*) validationFiles[ fileName.str().c_str() ]->Get( "DCHValidationHistograms/FinalStateLeptons" ) ) );
    
    
    if(i==0) color = 1;
    else if(i==1) color = 2;
    else if(i==2) color = 3;
    else if(i==3) color = 4;
    else if(i==4) color = 5;
    else if(i==5) color = 6;
    else if(i==6) color = 7;
    else if(i==7) color = 8;
    else if(i==8) color = 9;
    else if(i==9) color = 28;
    else if(i==10) color = 46;
    
    
    
    if(i==0){
    //Decay  Type
      c1->cd();
      h1[ fileName.str() ]->Draw();
      h1[ fileName.str() ]->SetLineColor(color);
      leg->AddEntry( h1[ fileName.str() ], "m(H^{#pm#pm}) = 300 GeV", "l");
      leg2->AddEntry( h1[ fileName.str() ], "m(H^{#pm#pm}) = 300 GeV", "l");
      
  
			h1[ fileName.str() ]->SetNameTitle("DecayType","H^{#pm#pm} #rightarrow l^{#pm}l^{#pm} Decay Modes");
			h1[ fileName.str() ]->GetYaxis()->SetTitle("Events");
			h1[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
		
			h1[ fileName.str() ]->GetXaxis()->SetLabelSize(0.06);
			h1[ fileName.str() ]->GetXaxis()->SetBinLabel(1,"Other");
			h1[ fileName.str() ]->GetXaxis()->SetBinLabel(2,"2e");
			h1[ fileName.str() ]->GetXaxis()->SetBinLabel(3,"2m");
			h1[ fileName.str() ]->GetXaxis()->SetBinLabel(4,"2t");
			h1[ fileName.str() ]->GetXaxis()->SetBinLabel(5,"em");
			h1[ fileName.str() ]->GetXaxis()->SetBinLabel(6,"tm");
			h1[ fileName.str() ]->GetXaxis()->SetBinLabel(7,"te");
			h1[ fileName.str() ]->GetXaxis()->SetBinLabel(8,"WW");      
      
      //4-leptonfinalstate
      
      c14->cd();
            
      h14[ fileName.str() ]->Draw();
      h14[ fileName.str() ]->SetLineColor(color);
      h14[ fileName.str() ]->SetNameTitle("FinalStateLeptons","H^{#pm#pm}H^{#mp#mp} #rightarrow l^{#pm}l^{#pm}l^{#mp}l^{#mp} 4-lepton final states");
      
      
      h14[ fileName.str() ]->GetYaxis()->SetTitle("Events");
 		  h14[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
  		h14[ fileName.str() ]->GetXaxis()->SetTitle("4-lepton final states");
  
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(1,"Other");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(2,"4e");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(3,"2e/2m");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(4,"2e/2t");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(5,"2e/em");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(6,"2e/tm");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(7,"2e/te");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(8,"4m");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(9,"2m/2t");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(10,"2m/em");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(11,"2m/tm");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(12,"2m/te");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(13,"4t");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(14,"2t/em");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(15,"2t/tm");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(16,"2t/te");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(17,"em/em");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(18,"em/tm");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(19,"em/te");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(20,"tm/tm");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(21,"te/tm");
			h14[ fileName.str() ]->GetXaxis()->SetBinLabel(22,"te/te");
      
      
      //c2->cd();
      //h2[ fileName.str() ]->Draw();

      //c3->cd();
      //h3[ fileName.str() ]->Draw();

			//Invariant mass positive charge
      c4->cd();
      h4[ fileName.str() ]->Draw();
      h4[ fileName.str() ]->SetLineColor(color);
      h4[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h4[ fileName.str() ]->GetXaxis()->SetTitle("m(l^{+}l^{+}) [GeV]");
      std::ostringstream ppm; ppm << "Pairs/(" << h4[ fileName.str() ]->GetBinWidth(0) << " GeV)";
      h4[ fileName.str() ]->GetYaxis()->SetTitle(ppm.str().c_str());

      c5->cd();
      h5[ fileName.str() ]->Draw();
      h5[ fileName.str() ]->SetLineColor(color);
      h5[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h5[ fileName.str() ]->GetXaxis()->SetTitle("m(l^{-}l^{-}) [GeV]");
      std::ostringstream ppn; ppn << "Pairs/(" << h5[ fileName.str() ]->GetBinWidth(0) << " GeV)";
      h5[ fileName.str() ]->GetYaxis()->SetTitle(ppn.str().c_str());

      c6->cd();
      h6[ fileName.str() ]->Draw();
      h6[ fileName.str() ]->SetLineColor(color);
      h6[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h6[ fileName.str() ]->GetXaxis()->SetTitle("p_{T} [GeV]");
      std::ostringstream pllp; pllp << "Leptons/(" << h6[ fileName.str() ]->GetBinWidth(0) << " GeV)";
      h6[ fileName.str() ]->GetYaxis()->SetTitle(pllp.str().c_str());

      c7->cd();
      h7[ fileName.str() ]->Draw();
      h7[ fileName.str() ]->SetLineColor(color);
      h7[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h7[ fileName.str() ]->GetXaxis()->SetTitle("p_{T} [GeV]");
      std::ostringstream nllp; nllp << "Leptons/(" << h7[ fileName.str() ]->GetBinWidth(0) << " GeV)";
      h7[ fileName.str() ]->GetYaxis()->SetTitle(nllp.str().c_str());

      c8->cd();
      h8[ fileName.str() ]->Draw();
      h8[ fileName.str() ]->SetLineColor(color);      
      h8[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h8[ fileName.str() ]->GetXaxis()->SetTitle("p_{T} [GeV]");
      std::ostringstream psllp; psllp << "Leptons/(" << h8[ fileName.str() ]->GetBinWidth(0) << " GeV)";
      h8[ fileName.str() ]->GetYaxis()->SetTitle(psllp.str().c_str());

      c9->cd();
      h9[ fileName.str() ]->Draw();
      h9[ fileName.str() ]->SetLineColor(color);
      h9[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h9[ fileName.str() ]->GetXaxis()->SetTitle("p_{T} [GeV]");
      std::ostringstream nsllp; nsllp << "Leptons/(" << h9[ fileName.str() ]->GetBinWidth(0) << " GeV)";
      h9[ fileName.str() ]->GetYaxis()->SetTitle(nsllp.str().c_str());

      c10->cd();
      h10[ fileName.str() ]->Draw();
      h10[ fileName.str() ]->SetLineColor(color);
      
      h10[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h10[ fileName.str() ]->GetXaxis()->SetTitle("#eta");
      std::ostringstream plle; plle << "Leptons/(" << h10[ fileName.str() ]->GetBinWidth(0) << " )";
      h10[ fileName.str() ]->GetYaxis()->SetTitle(plle.str().c_str());
      h10[ fileName.str() ]->GetYaxis()->SetRangeUser(0,1800);

      c11->cd();
      h11[ fileName.str() ]->Draw();
      h11[ fileName.str() ]->SetLineColor(color);
      h11[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h11[ fileName.str() ]->GetXaxis()->SetTitle("#eta");
      std::ostringstream nlle; nlle << "Leptons/(" << h11[ fileName.str() ]->GetBinWidth(0) << " )";
      h11[ fileName.str() ]->GetYaxis()->SetTitle(nlle.str().c_str());
      h11[ fileName.str() ]->GetYaxis()->SetRangeUser(0,1800);

      c12->cd();
      h12[ fileName.str() ]->Draw();
      h12[ fileName.str() ]->SetLineColor(color);
      h12[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h12[ fileName.str() ]->GetXaxis()->SetTitle("#eta");
      std::ostringstream pslle; pslle << "Leptons/(" << h12[ fileName.str() ]->GetBinWidth(0) << " )";
      h12[ fileName.str() ]->GetYaxis()->SetTitle(pslle.str().c_str());
      h12[ fileName.str() ]->GetYaxis()->SetRangeUser(0,1200);


      c13->cd();
      h13[ fileName.str() ]->Draw();
      h13[ fileName.str() ]->SetLineColor(color);      
      h13[ fileName.str() ]->GetYaxis()->SetTitleOffset(1.4);
      h13[ fileName.str() ]->GetXaxis()->SetTitle("#eta");
      std::ostringstream nslle; nslle << "Leptons/(" << h13[ fileName.str() ]->GetBinWidth(0) << " )";
      h13[ fileName.str() ]->GetYaxis()->SetTitle(nslle.str().c_str());
      h13[ fileName.str() ]->GetYaxis()->SetRangeUser(0,1200);
    }
    else{
    
    
    	c1->cd();
  	  h1[ fileName.str() ]->Draw("same");
      h1[ fileName.str() ]->SetLineColor(color);
    	    	   		  
      
      
      std::ostringstream legName; legName << "m(H^{#pm#pm}) = " << m_dch[i] << " GeV";
      leg->AddEntry( h1[ fileName.str() ], legName.str().c_str(), "l");
      leg2->AddEntry( h1[ fileName.str() ], legName.str().c_str(), "l");
      if(i==10) leg->Draw(); legend->Draw();
      //c2->cd();
      //h2[ fileName.str() ]->Draw("same");
      //h2[ fileName.str() ]->SetLineColor(color);
      //c3->cd();
      //h3[ fileName.str() ]->Draw("same");
      //h3[ fileName.str() ]->SetLineColor(color);
      c4->cd();
      h4[ fileName.str() ]->Draw("same");
      h4[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg2->Draw();
      c5->cd();
      h5[ fileName.str() ]->Draw("same");
      h5[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg2->Draw();
      c6->cd();
      h6[ fileName.str() ]->Draw("same");
      h6[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg2->Draw();
      c7->cd();
      h7[ fileName.str() ]->Draw("same");
      h7[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg2->Draw();
      c8->cd();
      h8[ fileName.str() ]->Draw("same");
      h8[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg2->Draw();
      c9->cd();
      h9[ fileName.str() ]->Draw("same");
      h9[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg2->Draw();
      c10->cd();
      h10[ fileName.str() ]->Draw("same");
      h10[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg->Draw();
      c11->cd();
      h11[ fileName.str() ]->Draw("same");
      h11[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg->Draw();
      c12->cd();
      h12[ fileName.str() ]->Draw("same");
      h12[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg->Draw();
      c13->cd();
      h13[ fileName.str() ]->Draw("same");
      h13[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg->Draw();
      
      c14->cd();
      h14[ fileName.str() ]->Draw("same");
      h14[ fileName.str() ]->SetLineColor(color);
      if(i==10) leg->Draw(); legend->Draw();
    }

  }
  c1->Print("ValidationPlots/GeneralPlots/DCH_DecayType.pdf");
  //c2->Print();
  //c3->Print();
  c4->Print("ValidationPlots/GeneralPlots/DCH_InvariantMassP.pdf");
  c5->Print("ValidationPlots/GeneralPlots/DCH_InvariantMassN.pdf");
  c6->Print("ValidationPlots/GeneralPlots/DCH_LeadingPtP.pdf");
  c7->Print("ValidationPlots/GeneralPlots/DCH_LeadingPtN.pdf");
  c8->Print("ValidationPlots/GeneralPlots/DCH_SubLeadingPtP.pdf");
  c9->Print("ValidationPlots/GeneralPlots/DCH_SubLeadingPtN.pdf");
  c10->Print("ValidationPlots/GeneralPlots/DCH_LeadingEtaP.pdf");
  c11->Print("ValidationPlots/GeneralPlots/DCH_LeadingEtaN.pdf");
  c12->Print("ValidationPlots/GeneralPlots/DCH_SubLeadingEtaP.pdf");
  c13->Print("ValidationPlots/GeneralPlots/DCH_SubLeadingEtaN.pdf");
  c14->Print("ValidationPlots/GeneralPlots/DCH_FinalStateLeptons.pdf");

}
