# echo "cleanup DCHValidation DCHValidation-00-00-00 in /afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.3/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtDCHValidationtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtDCHValidationtempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=DCHValidation -version=DCHValidation-00-00-00 -path=/afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL  $* >${cmtDCHValidationtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=DCHValidation -version=DCHValidation-00-00-00 -path=/afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL  $* >${cmtDCHValidationtempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtDCHValidationtempfile}
  unset cmtDCHValidationtempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtDCHValidationtempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtDCHValidationtempfile}
unset cmtDCHValidationtempfile
return $cmtcleanupstatus

