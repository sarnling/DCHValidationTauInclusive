###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
theApp.EvtMax = -1
MessageSvc.defaultLimit = 9999999  # all messages 
#--------------------------------------------------------------
# Load POOL support
#--------------------------------------------------------------
import AthenaPoolCnvSvc.ReadAthenaPool
svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL/run/test_DCH_A14NNPDF23LO_photospp.EVNT.pool.root"]
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL/run/test_DCH_A14NNPDF23LO.EVNT.pool.root"]

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Load "user algorithm"
#top algorithms to be run, and the libraries that house them
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

# Define your output file name and stream name (ROOT HISTO)
rootStreamName = 'DCHValidationStream'
rootFileName   = 'DCHValidation.photospp.histo.root'
#rootFileName   = 'DCHValidation.histo.root'

alg = CfgMgr.DCHValidationAlg( "DCHValidationAlg",
                               RootStreamName = rootStreamName,
                               RootDirName    = '/DCHValidationHistograms',
                               DCHmass        = 300.0
                               )


job += alg

# ====================================================================
# Define your output root file using MultipleStreamManager
# ====================================================================
from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
HistoXAODStream = MSMgr.NewRootStream( rootStreamName, rootFileName )
