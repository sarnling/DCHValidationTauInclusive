###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
theApp.EvtMax = -1
MessageSvc.defaultLimit = 10  # all messages 

#m_dch = 300   This is Set with  ...  athena --command m_dch = xxx ...

#--------------------------------------------------------------
# Load POOL support
#--------------------------------------------------------------
import AthenaPoolCnvSvc.ReadAthenaPool

# Define your input EVNT file path
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/s/sarnling/DCH_EVNT_FINAL/user.sarnling.13TeV.MC16.AtLeastOneTauFilter.DCH" + str(m_dch) + "_EXT0/user.sarnling.root"]
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/s/sarnling/DCH300NoTauLep.EVNT.pool.root"]
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/s/sarnling/DCH300NoTauHad.EVNT.pool.root"]
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/s/sarnling/DCH300MixedTauHadLep.EVNT.pool.root"]
svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/s/sarnling/DCH300FourTauHad.EVNT.pool.root"]

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Load "user algorithm"
#top algorithms to be run, and the libraries that house them
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

# Define your output file name and stream name (ROOT HISTO)
rootStreamName = 'DCHValidationStream'

#rootFileName   = '/afs/cern.ch/user/s/sarnling/DCHSignalSample/histograms/DCH' + str(m_dch) + 'AtLeastOneTauFilter.UndecayedTaus.Validation.histo.root'
#rootFileName   = '/afs/cern.ch/user/s/sarnling/DCHSignalSample/histograms/DCH300NoTauLep.Validation.histo.root'
#rootFileName   = '/afs/cern.ch/user/s/sarnling/DCHSignalSample/histograms/DCH300NoTauHad.Validation.histo.root'
#rootFileName   = '/afs/cern.ch/user/s/sarnling/DCHSignalSample/histograms/DCH300MixedTauHadLep.Validation.histo.root'
rootFileName   = '/afs/cern.ch/user/s/sarnling/DCHSignalSample/histograms/DCH300FourTauHad.Validation.histo.root'


alg = CfgMgr.DCHValidationAlg( "DCHValidationAlg",
                               RootStreamName = rootStreamName,
                               RootDirName    = '/DCHValidationHistograms',
                               DCHmass        = m_dch
                               )


job += alg

# ====================================================================
# Define your output root file using MultipleStreamManager
# ====================================================================
from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
HistoXAODStream = MSMgr.NewRootStream( rootStreamName, rootFileName )
