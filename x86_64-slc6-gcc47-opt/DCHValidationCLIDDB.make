#-- start of make_header -----------------

#====================================
#  Document DCHValidationCLIDDB
#
#   Generated Wed Oct  4 15:28:37 2017  by sarnling
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_DCHValidationCLIDDB_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_DCHValidationCLIDDB_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_DCHValidationCLIDDB

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationCLIDDB = $(DCHValidation_tag)_DCHValidationCLIDDB.make
cmt_local_tagfile_DCHValidationCLIDDB = $(bin)$(DCHValidation_tag)_DCHValidationCLIDDB.make

else

tags      = $(tag),$(CMTEXTRATAGS)

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationCLIDDB = $(DCHValidation_tag).make
cmt_local_tagfile_DCHValidationCLIDDB = $(bin)$(DCHValidation_tag).make

endif

include $(cmt_local_tagfile_DCHValidationCLIDDB)
#-include $(cmt_local_tagfile_DCHValidationCLIDDB)

ifdef cmt_DCHValidationCLIDDB_has_target_tag

cmt_final_setup_DCHValidationCLIDDB = $(bin)setup_DCHValidationCLIDDB.make
cmt_dependencies_in_DCHValidationCLIDDB = $(bin)dependencies_DCHValidationCLIDDB.in
#cmt_final_setup_DCHValidationCLIDDB = $(bin)DCHValidation_DCHValidationCLIDDBsetup.make
cmt_local_DCHValidationCLIDDB_makefile = $(bin)DCHValidationCLIDDB.make

else

cmt_final_setup_DCHValidationCLIDDB = $(bin)setup.make
cmt_dependencies_in_DCHValidationCLIDDB = $(bin)dependencies.in
#cmt_final_setup_DCHValidationCLIDDB = $(bin)DCHValidationsetup.make
cmt_local_DCHValidationCLIDDB_makefile = $(bin)DCHValidationCLIDDB.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)DCHValidationsetup.make

#DCHValidationCLIDDB :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'DCHValidationCLIDDB'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = DCHValidationCLIDDB/
#DCHValidationCLIDDB::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genCLIDDB_header
# Author: Paolo Calafiura
# derived from genconf_header

# Use genCLIDDB_cmd to create package clid.db files

.PHONY: DCHValidationCLIDDB DCHValidationCLIDDBclean

outname := clid.db
cliddb  := DCHValidation_$(outname)
instdir := $(CMTINSTALLAREA)/share
result  := $(instdir)/$(cliddb)
product := $(instdir)/$(outname)
conflib := $(bin)$(library_prefix)DCHValidation.$(shlibsuffix)

DCHValidationCLIDDB :: $(result)

$(instdir) :
	$(mkdir) -p $(instdir)

$(result) : $(conflib) $(product)
	@$(genCLIDDB_cmd) -p DCHValidation -i$(product) -o $(result)

$(product) : $(instdir)
	touch $(product)

DCHValidationCLIDDBclean ::
	$(cleanup_silent) $(uninstall_command) $(product) $(result)
	$(cleanup_silent) $(cmt_uninstallarea_command) $(product) $(result)

#-- start of cleanup_header --------------

clean :: DCHValidationCLIDDBclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(DCHValidationCLIDDB.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

DCHValidationCLIDDBclean ::
#-- end of cleanup_header ---------------
